const mocha = require('mocha');
const chai = require ('chai');
const chaihttp = require('chai-http');

chai.use(chaihttp);
var should = chai.should();


describe('Tests de conectividad', () => {
  it('Google funciona', (done)=> {
    chai.request('http://www.google.es')
        .get('/')
        .end((err, res) => {
          console.log(res)
          done()
        })
  })
})

describe('Tests de API usuarios', () => {
  it('Raiz OK', (done)=> {
    chai.request('http://localhost:3000')
        .get('/apitechu/v1')
        .end((err, res) => {
          console.log(res)
          res.should.have.status(200)
          res.body.mensaje.should.be.eql("Bienvenido a mi API")
          done()
        })
  })
  it('Lista de usuarios', (done)=> {
    chai.request('http://localhost:3000')
        .get('/apitechu/v1/usuarios')
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.a('array')
          for (var i = 0; i < res.body.length; i++) {
            res.body[i].should.have.property('email')
            res.body[i].should.have.property('password')
          }
          done()
        })
  })
})
