var express = require('express')
var bodyParser = require('body-parser')
var app = express()

var requestJson = require('request-json')
app.use(bodyParser.json())

var port = process.env.PORT || 3000
var fs = require('fs')

app.listen(port)

console.log("API escuchando en el puerto" + port)


var urlMlabRaiz = "https://api.mlab.com/api/1/databases/apitechuvfb12ed/collections/"
var apiKey = "apiKey=5zTFmFoXB8txjvfwTLCXt6mCqPL-P_ea"
var clienteMlab = null

// alta de usuarios

/*app.post('/usuarios', function(req, res)
{
  var clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)
  clienteMlab.get('', function(err, resM, body) {
    if (!err){
      res.send(body)
      console.log("bodylength")
      console.log(body.length)
      var contador = body.length +1
      console.log("contador")
      console.log(contador)
//      app.post('/usuarios', function(req, res) {
        const logado = "false"
        var nuevoUsuario =
          '{' +
             '"idusuario":' + contador + ',' +
             '"nombre":' + req.headers.nombre + ',' +
             '"apellido:"' + req.headers.apellido + ',' +
             '"ciudad:"' + req.headers.ciudad + ',' +
             '"telefono"' + req.headers.telefono + ',' +
             '"email"' + req.headers.email + ',' +
             '"password"' + req.headers.password + ',' +
             '"logged"' + logado +
          '}'
        var datosaltausuario = JSON.parse(nuevoUsuario)
        console.log(datosaltausuario)
        clienteMlab.post('', datosaltausuario, function(err, resM, body) {
          if (!err){
            res.send("Ok")
          }
//          else{
//            res.send("KO")
//          }
        })
      }
    })
//    else {
//      res.send(err)
//    }

//  clienteMlab.get('', function(err, resM, body){
//    if (!err){
//      res.send(body)
//      console.log(body)
//    }
//  })
*/

app.get('/apitechu/v6/usuarios', function(req, res){
  var clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)
  clienteMlab.get('', function(err, resM, body){
    if (!err){
      res.send(body)
    }
  })
})

// dado un id saca toda la infomacion de ese id
/*
app.get('/apitechu/v5/usuarios/:id', function(req, res) {
  var id = req.params.id
  var query = 'q={"id":' + id + '}'
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey)
  clienteMlab.get('', function(err, resM, body) {
    if (!err) {
      if (body.length > 0)
        res.send(body[0])
      else {
        res.status(404).send('Usuario no encontrado')
      }
    }
  })
})
*/

// dado un id devuelve el nombre y apellidos que corresponda
app.get('/apitechu/v5/usuarios/:id', function(req, res) {
  var id = req.params.id
  //var query = 'q={"id":' + id + '}'
   var query = 'q={"id":' + id + '}&f={"nombre":1, "apellido":1, "_id":0}'
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey)
  clienteMlab.get('', function(err, resM, body) {
    if (!err) {
      if (body.length > 0)
//        res.send({"nombre":body[0].nombre, "apellido":body[0].apellido})
        res.send(body[0])
      else {
        res.status(404).send('Usuario no encontrado')
      }
    }
  })
})

// hacer login con un email y password
/*app.post('/apitechu/v5/login', function(req, res) {
   var email = req.headers.email
   var password = req.headers.password
   var query = 'q={"email":"' + email + '","password":"' + password + '"}'
   clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey)
   clienteMlab.get('', function(err, resM, body) {
     if (!err) {
       if (body.length == 1) // Login ok
         res.send({"login":"ok", "id":body[0].id, "nombre":body[0].nombre, "apellido":body[0].apellido})
       else {
         res.status(404).send('Usuario no encontrado')
       }
     }
   })
})
*/

// hacer login con un email y password y añadir el atributo logado al registro encontrado

app.post('/apitechu/v5/login', function(req, res) {
    var email = req.headers.email
    var password = req.headers.password
    var query = 'q={"email":"' + email + '","password":"' + password + '"}'
    console.log(query)
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey)
    clienteMlab.get('', function(err, resM, body) {
      console.log(err)
      console.log(resM)
      console.log(body)
      if (!err) {
        if (body.length == 1) // Login ok
        {
          clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios")
          var cambio = '{"$set":{"logged":true}}'
          clienteMlab.put('?q={"id": ' + body[0].id + '}&' + apiKey, JSON.parse(cambio), function(errP, resP, bodyP) {
              res.send({"login":"ok", "id":body[0].id, "nombre":body[0].nombre, "apellidos":body[0].apellidos})
          })

        }
        else {
          res.status(404).send('Usuario no encontrado')
        }
      }
    })
})

//hacer el logout
app.post('/apitechu/v5/logout', function(req, res) {
    var id = req.headers.id
    var query = 'q={"id":' + id + ', "logged":true}'
    console.log(query)
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey)
    clienteMlab.get('', function(err, resM, body) {
      console.log(err)
      console.log(resM)
      console.log(body)
      if (!err) {
        if (body.length == 1) // Cliente estaba logado
        {
          clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios")
          var cambio = '{"$set":{"logged":false}}'
          clienteMlab.put('?q={"id": ' + body[0].id + '}&' + apiKey, JSON.parse(cambio), function(errP, resP, bodyP) {
              res.send({"logout":"ok", "id":body[0].id, "nombre":body[0].nombre, "apellidos":body[0].apellidos})
          })

        }
        else {
          res.status(404).send('Usuario no logado previamente')
        }
      }
    })
})

// obtener las cuentas (iban) de un cliente
app.get('/apitechu/v5/cuentas', function(req, res) {
    var idcliente = req.headers.id
    var query = 'q={"id cliente":' + idcliente + '}'
    var filter = 'f={"IBAN":1,"_id":0}'
    console.log(query)
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/Cuentas?" + query + "&" + filter + "&" + apiKey)
    clienteMlab.get('', function(err, resM, body) {
    if (!err) {
        if (body.length > 0) // Cliente encontrado
        {
          res.send(body)
//         var ibanarray = []
//          for (var i = 0; i < body.length; i++) {
//            console.log(body[i].IBAN)
//            ibanarray.push(body[i].IBAN)
//          }
//            res.send(ibanarray)
        }
        else {
          res.status(404).send("identificador no encontrado")
        }
      }
    })
})

// obtener las contratos de un cliente
app.get('/apitechu/v5/contrato', function(req, res) {
    var idcliente = req.headers.id
    var query = 'q={"idcliente":' + idcliente + '}'
    var filter = 'f={"contrato.iban":1,"_id":0}'
    console.log(query)
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/Contratos?" + query + "&" + filter + "&" + apiKey)
    clienteMlab.get('', function(err, resM, body) {
    if (!err) {
        if (body.length > 0) // Cliente encontrado
        {
          res.send(body)
//         var ibanarray = []
//          for (var i = 0; i < body.length; i++) {
//            console.log(body[i].IBAN)
//            ibanarray.push(body[i].IBAN)
//          }
//            res.send(ibanarray)
        }
        else {
          res.status(404).send("identificador no encontrado")
        }
      }
    })
})

// obtener las contratos de cuentas vista de un cliente
app.get('/apitechu/v5/contrato/vista', function(req, res) {
    var idcliente = req.headers.id
    var query = 'q={"idcliente":' + idcliente + '}'
    var filter = 'f={"iban":1,"saldo":2,"_id":0}'
    console.log(query)
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/Cuentas?" + query + "&" + filter + "&" + apiKey)
    clienteMlab.get('', function(err, resM, body) {
    if (!err) {
        if (body.length > 0) // Cliente encontrado
        {
          res.send(body)
//         var ibanarray = []
//          for (var i = 0; i < body.length; i++) {
//            console.log(body[i].IBAN)
//          }
//            res.send(ibanarray)
        }
        else {
          res.status(404).send("identificador no encontrado")
        }
      }
    })
})

// obtener las contratos de tarjetas de un cliente
app.get('/apitechu/v5/contrato/tarjeta', function(req, res) {
  var idcliente = req.headers.id
  var query = 'q={"idcliente":' + idcliente + '}'
  var filter = 'f={"iban":1,"pan":2,"saldo":3,"_id":0}'
  console.log(query)
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/Tarjetas?" + query + "&" + filter + "&" + apiKey)
  clienteMlab.get('', function(err, resM, body) {
  if (!err) {
      if (body.length > 0) // Cliente encontrado
      {
        res.send(body)
//         var ibanarray = []
//          for (var i = 0; i < body.length; i++) {
//            console.log(body[i].IBAN)
//          }
//            res.send(ibanarray)
      }
      else {
        res.status(404).send("identificador no encontrado")
      }
    }
  })
})

// obtener las contratos de prestamos de un cliente
app.get('/apitechu/v5/contrato/prestamo', function(req, res) {
  var idcliente = req.headers.id
  var query = 'q={"idcliente":' + idcliente + '}'
  var filter = 'f={"iban":1,"capitalpendiente":2,"_id":0}'
  console.log(query)
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/Prestamos?" + query + "&" + filter + "&" + apiKey)
  clienteMlab.get('', function(err, resM, body) {
  if (!err) {
      if (body.length > 0) // Cliente encontrado
      {
        res.send(body)
//         var ibanarray = []
//          for (var i = 0; i < body.length; i++) {
//            console.log(body[i].IBAN)
//          }
//            res.send(ibanarray)
      }
      else {
        res.status(404).send("identificador no encontrado")
      }
    }
  })
})

// obtener los movimientos de un contrato de vista

app.get('/apitechu/v5/contrato/vista/movimientos', function(req, res) {
  var iban = req.headers.idcuenta
  var idcliente = req.headers.id
  var query = 'q={"idcliente":' + idcliente +', "iban":' + iban + '}'
  var filter = 'f={"movimientos":1,"_id":0}'
  console.log(query)
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/Cuentas?" + query + "&" + filter + "&" + apiKey)
  console.log(clienteMlab)
  clienteMlab.get('', function(err, resM, body) {
  if (!err) {
      console.log(body)
      console.log(err)
      console.log(resM)
      if (body.length > 0) // Cliente encontrado
      {
        res.send(body)
//         var ibanarray = []
//          for (var i = 0; i < body.length; i++) {
//            console.log(body[i].IBAN)
//            ibanarray.push(body[i].IBAN)
//          }
//            res.send(ibanarray)
      }
      else {
        res.status(404).send("identificador no encontrado")
      }
    }
  })
})

// obtener los movimientos de un contrato de tarjeta

app.get('/apitechu/v5/contrato/tarjeta/movimientos', function(req, res) {
  var iban = req.headers.idcuenta
  var idcliente = req.headers.id
  var query = 'q={"idcliente":' + idcliente +', "iban":' + iban + '}'
  var filter = 'f={"movimientos":1,"_id":0}'
  console.log(query)
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/Tarjetas?" + query + "&" + filter + "&" + apiKey)
  console.log(clienteMlab)
  clienteMlab.get('', function(err, resM, body) {
  if (!err) {
      console.log(body)
      console.log(err)
      console.log(resM)
      if (body.length > 0) // Cliente encontrado
      {
        res.send(body)
//         var ibanarray = []
//          for (var i = 0; i < body.length; i++) {
//            console.log(body[i].IBAN)
//            ibanarray.push(body[i].IBAN)
//          }
//            res.send(ibanarray)
      }
      else {
        res.status(404).send("identificador no encontrado")
      }
    }
  })
})

// obtener los movimientos de un contrato de préstamo

app.get('/apitechu/v5/contrato/prestamo/movimientos', function(req, res) {
  var iban = req.headers.idcuenta
  var idcliente = req.headers.id
  var query = 'q={"idcliente":' + idcliente +', "iban":' + iban + '}'
  var filter = 'f={"movimientos":1,"_id":0}'
  console.log(query)
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/Prestamos?" + query + "&" + filter + "&" + apiKey)
  console.log(clienteMlab)
  clienteMlab.get('', function(err, resM, body) {
  if (!err) {
      console.log(body)
      console.log(err)
      console.log(resM)
      if (body.length > 0) // Cliente encontrado
      {
        res.send(body)
//         var ibanarray = []
//          for (var i = 0; i < body.length; i++) {
//            console.log(body[i].IBAN)
//            ibanarray.push(body[i].IBAN)
//          }
//            res.send(ibanarray)
      }
      else {
        res.status(404).send("identificador no encontrado")
      }
    }
  })
})

// dar de alta un prestamo

/*app.post('/crearprestamo', function(req, res){
  //console.log("Vamos a crear las cuentas del nuevo usuario " + req.body.idusuario);

  var fecha = new Date().toJSON().slice(0,10)

  //console.log(fecha);

  var fechaeditada = "";
  fechaeditada = fecha.substring(8,10) + "/"
               + fecha.substring(5,7) + "/"
               + fecha.substring(0,4)

  //console.log(fechaeditada);

  var saldoprestamo ""



  var nuevoPrestamo =
    '{' +
       '"idcliente":' + req.headers.id + ',' +
       '"iban":"' + req.body.iban + '-1",' +
       '"movimientos":[' +
               '{' +
                   '"id":1.00,' +
                   '"fecha":"' + fechaeditada + '",' +
                   '"importe":0.00,' +
                   '"concepto":"Apertura prestamo",' +
               '}' +
            ']' +
        '"capitalpendiente":''
         '},' +
         '{' +
            '"idcuenta":"' + req.body.idusuario + '-2",' +
            '"saldo":0.00,' +
            '"divisa":"EUR",' +
            '"descripcion":"Cuenta Ahorro",' +
            '"movimientos":[' +
               '{' +
                   '"fecha":"' + fechaeditada + '",' +
                   '"importe":0.00,' +
                   '"tipo":"Apertura cuenta",' +
                   '"descripcion":"Apertura cuenta",' +
                   '"saldomovimiento":0.00' +
               '}' +
            ']' +
         '}' +
       ']' +
    '}'

  //console.log(nuevasCuentas);
  var datosinsertmongo = JSON.parse(nuevoPrestamo);
  //console.log(datosinsertmongo);
  //console.log(urlMlab + "?" + apiKey);
  clienteMlab = requestJson.createClient(urlMlab + "?" + apiKey);
  clienteMlab.post('',datosinsertmongo, function(err, resM, body){
    if(err)
    {
      console.log(resM);
      res.send("KO");
    }
    else
    {
      res.send("OK");
    }
  });
});
*/

app.post('/apitechu/v5/contrato/prestamo', function(req, res) {
//    var query = 'q={"idcliente":"' + idcliente + '"}'
//    console.log(query)
//  var idcliente = req.headers.id
//  var query = 'q={"idcliente":' + idcliente + '}'
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/Prestamos?" + apiKey)
//  req.body.id = idcliente
//clienteMlab = requestJson.createClient(urlMlabRaiz + "/Prestamos?" +  query + "&" + apiKey)
  clienteMlab.post('', req.body, function(err, resM, body){
    res.send({"Prestamo creado": "OK","body":req.body,"cuenta":req.body.iban})
    console.log(body)
  })
})

app.post('/apitechu/v1/contrato/prestamo', function(req, res) {
//    var query = 'q={"idcliente":"' + idcliente + '"}'
//    console.log(query)
//  var idcliente = req.headers.id
//  var query = 'q={"idcliente":' + idcliente + '}'
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/Prestamos?" + apiKey)
//  req.body.id = idcliente
//clienteMlab = requestJson.createClient(urlMlabRaiz + "/Prestamos?" +  query + "&" + apiKey)
  clienteMlab.post('', req.body, function(errb, resM, body){
    res.send({"Prestamo creado": "OK","body":req.body,"cuenta":req.body.iban})
    console.log(body)
  })
})



//borrar un movimiento de tarjeta

app.delete('/apitechu/v5/contrato/tarjeta', function(req, res){
  var idcliente = req.headers.id
  console.log(idcliente)
  var iban = req.headers.iban
  var query = 'q={"idcliente":' + idcliente +', "iban":' + iban + '}'
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/Tarjetas?" +  query + "&" + apiKey)
  console.log(clienteMlab)
  clienteMlab.delete('', function(err, resM, body) {
  if (!err) {
      console.log(body)
      console.log(err)
      console.log(resM)
      if (body.length > 0) // Cliente encontrado
      {
        res.send(body)
      }
      else {
        res.status(404).send("identificador no encontrado")
      }
    }
  })
})


/*    clienteMlab.get('', function(err, resM, body) {
      console.log(err)
      console.log(resM)
      console.log(body)
      if (!err) {
        if (body.length == 1) // Login ok
        {
          clienteMlab = requestJson.createClient(urlMlabRaiz + "/Prestamos")
          var cambio = '{"$set":{"logged":true}}'
          clienteMlab.put('?q={"id": ' + body[0].id + '}&' + apiKey, JSON.parse(cambio), function(errP, resP, bodyP) {
              res.send({"login":"ok", "id":body[0].id, "nombre":body[0].nombre, "apellidos":body[0].apellidos})
          })

        }
        else {
          res.status(404).send('Usuario no encontrado')
        }
      }
    })
})
*/


// obtener los movimientos de una cuenta

app.get('/apitechu/v5/cuentas/movimientos', function(req, res) {
  var iban = req.headers.idcuenta
  var query = 'q={"IBAN":' + iban + '}'
  var filter = 'f={"movimientos":1,"_id":0}'
  console.log(query)
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/Cuentas?" + query + "&" + filter + "&" + apiKey)
  console.log(clienteMlab)
  clienteMlab.get('', function(err, resM, body) {
  if (!err) {
      console.log(body)
      console.log(err)
      console.log(resM)
      if (body.length > 0) // Cliente encontrado
      {
        res.send(body)
//         var ibanarray = []
//          for (var i = 0; i < body.length; i++) {
//            console.log(body[i].IBAN)
//            ibanarray.push(body[i].IBAN)
//          }
//            res.send(ibanarray)
      }
      else {
        res.status(404).send("identificador no encontrado")
      }
    }
  })
})

app.get('/apitechu/v5/usuarios/:id', function(req, res)
{
  var id = req.params.id
  var query = 'q={"id:":' + id + '}'
  var clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&" + apiKey)
  console.log(clienteMlab)
  clienteMlab.get('', function(err, resM, body){
    console.log(err)
    if (!err){
      if (body.length > 0)
          res.send(body[0])
      else{
          res.status(404).send("Usuario no encontrado")
      }
    }
  })
})
// fin - cambios añadidos el 19-02-2018

/*
var usuarios = require('./usuarios-ejercicio.json')
//var usuarios = require('./usuarios.json')
//console.log("Hola mundo")

app.get('/apitechu/v1', function(req, res)
{
  //console.log(req)
  res.send({"mensaje":"Bienvenido a mi API"})
})

app.get('/apitechu/v1/usuarios', function(req, res)
{
  res.send(usuarios)
})


app.post('/apitechu/v1/usuarios', function(req, res)
{
  var nuevo = {"first_name":req.headers.first_name,
               "country":req.headers.country}
  usuarios.push(nuevo)
  console.log(req.headers)
  const datos = JSON.stringify(usuarios)
  fs.writeFile("./usuarios.json", datos, "utf8", function(err)
  {
    if (err){
        console.log(err)

    } else {
        console.log("Fichero guardado")
    }
  })
  res.send("Alta ok")

})

app.post('/apitechu/v2/usuarios', function(req, res)
{
  //var nuevo = {"first_name":req.headers.first_name,
  //             "country":req.headers.country}
  var nuevo = req.body
  usuarios.push(nuevo)
  //console.log(req.headers)
  const datos = JSON.stringify(usuarios)
  fs.writeFile("./usuarios.json", datos, "utf8", function(err)
  {
    if (err){
        console.log(err)

    } else {
        console.log("Fichero guardado")
    }
  })
  res.send("Alta ok")

})
//Borrado

splice es una funcion que establece que elemento se va a borrar y el número
que va a borrar


app.delete('/apitechu/v1/usuarios/:elegido', function(req, res)
 {
   usuarios.splice(req.params.elegido-1, 1)
   res.send("Usuario borrado")
 }
)

app.post('/apitechu/v1/monstruo/:p1/:p2', function (req, res){
  console.log("Parametros")
  console.log(req.params)
  console.log("QueryString")
  console.log(req.query)
  console.log("Headers")
  console.log(req.headers)
  console.log("Body")
  console.log(req.body)
  res.send("Alta de todo tipo")
})

app.post('/apitechu/v3/usuarios/login', function(req, res)
{
  var email = req.headers.email
  var password = req.headers.password
  var idusuario = 0

  for (var i = 0; i < usuarios.length; i++) {
    if (usuarios[i].email == email && usuarios[i].password == password)
    {
      idusuario = usuarios[i]
      usuarios[i].logged = true
      break;
    }

  }
//res.send("Usuario encontrado " + idusuario)
  if (idusuario != 0){
      res.send({"usuario encontrado":"SI", "ID":idusuario})
    }else{
      res.send("Usuario no encontrado")
    }

})

app.post('/apitechu/v3/usuarios/logout', function(req, res)
{
    var idusuario = req.headers.id
    var usuario_logado = false

  for (var i = 0; i < usuarios.length; i++) {
    if (usuarios[i].id == idusuario && usuarios[i].logged == true)
    {
      usuario_logado = true
      usuarios[i].logged = false
      break;
    }

  }
//res.send("Usuario encontrado " + idusuario)
  if (usuario_logado){
      res.send({"logout":"SI", "ID":idusuario})
    }else{
      res.send({"logout":"NO", "MSJ":"El usuario no estaba logado", "ID":idusuario})
    }

})

// api de cuentas

var cuentas = require('./cuentas.json')

app.get('/apitechu/v1/cuentas', function(req, res)
{
  res.send(cuentas)
})

app.get('/apitechu/v1/cuentas/iban', function(req, res)
{

  var ibanarray = []
  for (var i = 0; i < cuentas.length; i++) {

      ibanarray.push(cuentas[i].IBAN)

    }
   res.send({"Iban encontrado":"SI", "IBAN":ibanarray})


})

app.get('/apitechu/v1/cuentas/movimientos', function(req, res)
{
//  for (var i = 0; i < cuentas.length; i++) {
//    res.send(cuentas[i].IBAN)
//}
var iban = req.headers.IBAN
//usuarios.push(nuevo)
console.log(req.headers)
var movimientosarray = []

  for (var i = 0; i < cuentas.length; i++) {
      if (iban == cuentas[i].IBAN){
        movimientosarray.push(cuentas[i].movimientos)
       }
      }
 res.send(movimientosarray)
})
*/
